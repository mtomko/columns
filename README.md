# Columns #
A simple command line utility for exploring tabular data files.


## Usage ##
```
Prints the columns at a specific row in a tabular file

  columns [FILENAME]

=== flags ===

  [-r int]       Row number (1-based)
  [-build-info]  print info about this build and exit
  [-version]     print the version of this build and exit
  [-help]        print this help text and exit
                 (alias: -?)
```
