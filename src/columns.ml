open Core.Std

let split line =
  String.split line ~on:'\t'

let transpose elts =
  String.concat ~sep:"\n" elts

let rec columns channel row =
  if row = 0 then
     let line = In_channel.input_line channel in
     match line with
     | Some l -> split l
     | None -> []
  else 
    let _ = In_channel.input_line channel in
    columns channel (row - 1)

let get_channel = function
  | None | Some "-" -> In_channel.stdin
  | Some filename -> In_channel.create ~binary:false filename

let do_columns filename row_number () =
  let channel = get_channel filename in
  let cols = columns channel (row_number - 1) in
  print_endline (transpose cols)

let command =
  Command.basic
    ~summary:"Prints the columns at a specific row in a tabular file"
    Command.Spec.(
      empty 
      +> anon (maybe ("filename" %: file))
      +> flag "-r" (optional_with_default 1 int) ~doc:"int Row number (1-based)"
    )
    do_columns

let () =
  Command.run ~version:"1.0" command
